var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multiparty = require('multiparty');
var mongoose = require('mongoose');

var app = express();

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

var passport = require('passport');
var config = require('./config/configurations');

var db = process.env.MONGODB || 'mongodb://localhost/headstart-dev';
var connection = mongoose.connect(db).connection;
//mongoose.connect('mongodb://dbuser:dbuser1234@ds031792.mongolab.com:31792/headstart-higher');
//mongoose.connect('mongodb://dbuser:dbuser1234@ds055792.mongolab.com:55792/headstart-hyd');
connection.on('error', function(err) { console.log(err.message); });
connection.once('open', function() {
  console.log("mongodb connection open");
});

require('./config/passport')(passport);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'foobarserverissecret',
    name: 'sid',
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));
app.use(passport.initialize());
app.use(passport.session());

require('./app/routes/index')(app, passport);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if(app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

config();

module.exports = app;

