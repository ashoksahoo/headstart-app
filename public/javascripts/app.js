var shortList = function(id) {
    $.get("/shortlist/" + id, function() {
        $('#star' + id)
            .removeClass("glyphicon-star-empty").addClass("glyphicon-star")
            .attr("onclick", "").unbind('click').bind('click', id, function(event) {
                removeShortList(event.data)
            }
        );
    }).fail(
        function(err) {
            alert(JSON.stringify(err.responseText))
        }
    );
};
var removeShortList = function(id) {
    $.get("/remove-shortlist/" + id, function() {
        $('#star' + id)
            .addClass("glyphicon-star-empty").removeClass("glyphicon-star")
            .attr("onclick", "").unbind('click').bind('click', id, function(event) {
                shortList(event.data)
            }
        );

    }).fail(
        function(err) {
            alert(err.responseText)
        }
    );
};