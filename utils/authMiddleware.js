/**
 * Created by Ashok on 04-05-2015.
 */
var onUnAuthorizedAccess;

var isAdmin;

onUnAuthorizedAccess = function(req, res) {
    return res.redirect('/login');
};
isAdmin = function(req, res, next) {
    if(req.session && req.session.user && req.session.user.role.code === 3) {
        next();
        return true;
    }
    return false;
};
exports.isAdmin  = function(req, res, next) {
    if(req.session && req.session.user && req.session.user.role.code === 3) {
        return next();
    }
    return onUnAuthorizedAccess(req, res);
};


exports.isCandidate = function(req, res, next) {
    if(req.session && req.session.user && req.session.user.role.code === 1) {
        return next();
    } else {
        if(isAdmin(req, res, next)) {
            return;
        }
        return onUnAuthorizedAccess(req, res);
    }
};

exports.isLoggedIn = function(req, res, next) {
    if(req.session && req.session.user) {
        next();

    } else {
        onUnAuthorizedAccess(req, res, true);
    }
};

exports.isStartup = function(req, res, next) {
    if(req.session && req.session.user && req.session.user.role.code === 2) {
        next();

    } else {
        if(isAdmin(req, res, next)) {
            return;
        }
        onUnAuthorizedAccess(req, res, true);
    }
};

