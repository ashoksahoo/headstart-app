/**
 * Created by Ashok on 12-05-2015.
 */
var User = require('../models/users');

var _ = require('lodash');
var async = require('async');
var multer = require('multer');
var upload = multer({dest: 'uploads/'});
var fs = require('fs');
var path = require('path');
var Papa = require('babyparse');
/*var FileReader = require('filereader');*/
/*var candidates = require('../../hyd-july/candidates.json');
 var startups = require('../../hyd-july/startups.json');*/

exports.uploadData = function(req, res) {
    /* _.forEach(candidates, function(candidate) {
     candidate.starred = [];
     User.create(candidate)
     });*/
    /* _.forEach(startups, function(startup) {
     startup.starred = [];
     User.create(startup)
     });*/
    res.sendStatus(200)
};
exports.uploadPage = function(req, res) {
    res.render('upload-page', {user: req.session.user})
};
exports.upload = function(req, res) {
    /*    console.log(req.files.candidates[0]);
     console.log(req.files.startups[0]);*/
    //res.redirect('/');
    function trim(str) {
        return str.replace(/\s/g, '');
    }

    function trimKeys(obj) {
        var o = obj;
        for(var k in o) {
            if(k.replace(/\s/g, '') != k && o.hasOwnProperty(k)) {
                o[k.replace(/\s/g, '')] = o[k];
                delete o[k];
            }
        }
        return o;
    }

    function Startup(config) {
        config = config || {};
        var thisObject = {};
        thisObject.user_role = {"name": "startup", "code": 2};
        thisObject.startup = {};
        thisObject.name = config.FirstName + " " + config.LastName;
        thisObject.mobile = config.MobileNumber || 0;
        thisObject.email = config.Email || "";
        var sQuestions = {
            "What do you call your Startup?": "startup_name",
            "Your Startup website URL?": "website",
            "Tell us about your startup!": "intro",
            "How old is the Start-Up?": "age",
            "Current Team Size? (Number of people)": "team_size",
            "Funding Status?": "funding_status",
            "Expected Experience Profile?": "experience",
            "Current Talent Requirements?": "requirements",
            "Number of positions you seek to fill?": "openings",
            "Specific qualities you seek in a candidate for your startup?": "roles",
            "What compensation packages do you offer prospective hires? ": "perks",
            "Where did you hear about us? ": "about_headstart",
            "Are you looking for a co-founder?": "cofounder"
        };
        var that = thisObject;
        for(var key in sQuestions) {
            that.startup[sQuestions[key]] = config[trim(key)];
        }
        return thisObject;
    }

    function Candidate(config) {
        config = config || {};
        var thisObject = {};
        thisObject.user_role = {"name": "candidate", "code": 1};
        thisObject.candidate = {};
        thisObject.name = config.FirstName + " " + config.LastName;
        thisObject.mobile = config.MobileNumber || 0;
        thisObject.email = config.Email || "";
        var cQuestions = {
            "What motivates you to work for a Startup?": "motivation",
            "Crisply share your 3 biggest professional achievements": "achivements",
            "Educational Qualifications?": "qualification",
            "Tell us more about your Education?": "education",
            "Which of these Roles suit you?": "roles",
            "Please specify your primary area of expertise in technology": "primary_expertise",
            "Please specify your secondary area of expertise in technology": "secondary_expertise",
            "Do you have a passion for something? Could you share a little about it? ": "passion",
            "Current Employer?": "employer",
            "Total Relevant Experience?": "experience",
            "What makes you eligible to work at a startup?": "eligibilty",
            "What is your ideal job profile?": "profile",
            "Please add your LinkedIn profile. ": "linkedin",
            "Where did you hear about us? ": "about_headstart",
            "Would you like to be a co-founder?": "cofounder"
        };
        //additional achivements, passion, primary_expertise, secondary_expertise
        for(var key in cQuestions) {
            thisObject.candidate[cQuestions[key]] = config[trim(key)];
        }
        return thisObject;
    }

    if(req.files.candidates) {
        fs.readFile(req.files.candidates[0].path, "utf8", function(error, data) {
            var data1 = Papa.parse(data, {header: true}).data,
                candidates = [];
            for(var i = 0; i < data1.length; i++) {
                var dataOne = trimKeys(data1[i]);
                if(trim(dataOne["Timestamp"])!=""){
                    var candidate = Candidate(dataOne);
                    candidate.starred = [];
                    User.create(candidate);
                    candidates.push(candidate);
                }
            }
            console.log(candidates.length);
        });
    }
    if(req.files.startups) {
        fs.readFile(req.files.startups[0].path, "utf8", function(error, data) {
            var data1 = Papa.parse(data, {header: true}).data,
                startups = [];
            for(var i = 0; i < data1.length; i++) {
                var dataOne = trimKeys(data1[i]);
                if(trim(dataOne["Timestamp"])!=""){
                    var startup = Startup(dataOne);
                    startup.starred = [];
                    User.create(startup);
                    startups.push(startup);
                }
            }
            console.log(startups.length);
        });
    }
    res.redirect('/');
};