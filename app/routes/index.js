var controller = require('../controllers/candidate');
var uploader = require('../services/uploader');
var authResponse = require('../../utils/authResponse');
var auth = require('../../utils/authMiddleware');
var multer = require('multer')
var upload = multer({dest: 'uploads/'})

/* GET home page. */

module.exports = function(app, passport) {
    app.get('/', auth.isLoggedIn, function(req, res, next) {
        if(req.session && req.session.user && req.session.user.role.code === 3) {
            controller.getCandidates(req, res, next);
        }
        if(req.session && req.session.user && req.session.user.role.code === 2) {
            controller.getCandidates(req, res, next);
        }
        if(req.session && req.session.user && req.session.user.role.code === 1) {
            controller.getStartups(req, res, next);
        }
    });
    //login
    app.get('/login', function(req, res, next) {
        res.render('login');
    });
    app.get('/admin-login', function(req, res, next) {
        res.render('admin-login');
    });
    app.get('/logout', function(req, res, next) {
        req.session.destroy();
        res.redirect('/login');
    });
    //candidates
    app.get('/candidates', auth.isStartup, controller.getCandidates);
    app.get('/candidate/:id', auth.isStartup, controller.getCandidate);
    app.post('/note/:id', auth.isLoggedIn, controller.saveNote);

    app.get('/edit-profile', auth.isLoggedIn, controller.getEditProfile);

    app.get('/profile', auth.isLoggedIn, controller.getProfile);

    app.get('/faq', auth.isLoggedIn, function(req, res) {
        res.render('faq', {user: req.session.user})
    });

    app.post('/profile', auth.isLoggedIn, controller.updateProfile);

    //startups
    app.get('/startups', auth.isCandidate, controller.getStartups);
    app.get('/startup/:id', auth.isCandidate, controller.getStartup);
    //Shortlist
    app.get('/shortlist/:id', auth.isLoggedIn, controller.shortList);
    app.get('/shortlisted', auth.isLoggedIn, controller.shortListed);
    app.get('/remove-shortlist/:id', auth.isLoggedIn, controller.removeShortList);

    app.post('/login', function(req, res, next) {
        req.body.password = "fakepassword";
        //This middlwware need a password field, which we are not using, so there is a fake password.
        return passport.authenticate('local', function(err, user, info) {
            return authResponse.loginResponse(req, res, err, user, info);
        })(req, res, next);
    });
    app.post('/admin-login', function(req, res, next) {
        return passport.authenticate('local', function(err, user, info) {
            return authResponse.loginResponse(req, res, err, user, info);
        })(req, res, next);
    });
    app.get('/upload-data', auth.isAdmin, uploader.uploadData);
    app.get('/upload-page', auth.isAdmin, uploader.uploadPage);
    app.post('/upload', auth.isAdmin, upload.fields([{name: 'candidates', maxCount: 1}, {
        name: 'startups',
        maxCount: 1
    }]), uploader.upload);
};