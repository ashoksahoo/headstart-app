mongoose = require("mongoose");
Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        trim: true,
        index: {unique: true}
    },
    mobile: {
        type: String,
        trim: true,
        index: {unique: true}
    },
    password: "",
    user_role: {},
    candidate: {},
    startup: {},
    starred: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', UserSchema);
