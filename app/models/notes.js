/**
 * Created by Ashok on 07-05-2015.
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var NoteSchema = new Schema({
    note: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    note_for: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Note', NoteSchema);
