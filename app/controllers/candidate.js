/**
 * Created by Ashok on 04-05-2015.
 */
var services = require('../services/candidate');
var _ = require('lodash');

exports.getCandidates = function(req, res, next) {
    var candidates;
    var user = req.session.user;
    var callback = function(err, obj) {
        var starred;
        if(err) {
            return res.status(500).send(err);
        }
        candidates = JSON.parse(JSON.stringify(obj.candidates));
        if(!obj.starred) {
            obj.starred = {starred: []};
        }
        starred = JSON.parse(JSON.stringify(obj.starred.starred));
        _.forEach(candidates, function(candidate, key) {
            if(_.find(starred, function(star) {
                        return candidate._id == star;
                    }
                )) {
                candidates[key].star = true;
            }
        });
        res.render('candidates', {users: candidates, user: req.session.user});
    };
    services.getCandidates(user, callback)

};

exports.getCandidate = function(req, res, next) {
    var id = req.params.id;
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).send(err);
        }
        if(obj.note) {
            return res.render('candidate', {candidate: obj.candidate, note: obj.note.note, user: req.session.user});
        }
        res.render('candidate', {candidate: obj.candidate, note: '', user: req.session.user});
    };
    services.getCandidate(id, user, callback)

};
exports.saveNote = function(req, res, next) {
    var user = req.session.user;
    var noteObj = {
        note: req.body.note,
        note_for: req.params.id,
        user: user.id
    };
    var callback = function(err, obj) {
        if(err) {
            console.error(err);
            return res.status(500).send(err);
        }
        if(user.role.code == 2) {
            res.redirect('/candidate/' + req.params.id);
        }
        if(user.role.code == 1) {
            res.redirect('/startup/' + req.params.id);
        }
        if(user.role.code == 3) {
            console.log(req.headers.referer)
            res.redirect(req.headers.referer);
        }
    };
    services.saveNote(noteObj, callback)

};
exports.getStartups = function(req, res, next) {
    var startups, starred;
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).send(err);
        }
        startups = JSON.parse(JSON.stringify(obj.startups));
        if(!obj.starred) {
            obj.starred = {starred: []};
        }
        starred = JSON.parse(JSON.stringify(obj.starred.starred));
        _.forEach(startups, function(startup, key) {
            if(_.find(starred, function(star) {
                        return startup._id == star;
                    }
                )) {
                startups[key].star = true;
            }
        });
        res.render('startups', {users: startups, user: req.session.user});
    };
    services.getStartups(user, callback)

};
exports.getStartup = function(req, res, next) {
    var id = req.params.id;
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).send(err);
        }
        if(obj.note) {
            return res.render('startup', {startup: obj.startup, note: obj.note.note, user: req.session.user});
        }
        res.render('startup', {startup: obj.startup, note: '', user: req.session.user});

    };
    services.getStartup(id, user, callback)

};
exports.getProfile = function(req, res, next) {
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).send(err);
        }
        res.render('profile', {profile: obj, user: req.session.user});
    };
    services.getProfile(user, callback);

};
exports.getEditProfile = function(req, res, next) {
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).send(err);
        }
        res.render('edit-profile', {profile: obj, user: req.session.user});
    };
    services.getProfile(user, callback);

};

exports.updateProfile = function(req, res, next) {
    var user = req.session.user;
    var profile;
    if(user.role.code == 1) {
        profile = {
            qualification: req.body.qualification,
            education: req.body.education,
            employer: req.body.employer,
            motivation: req.body.motivation,
            experience: req.body.experience,
            eligibility: req.body.eligibility,
            cofounder: req.body.cofounder,
            linkedin: req.body.linkedin,
            about_startup: req.body.about_startup
        };
    }
    else
        profile = {
            website: req.body.website,
            funding_status: req.body.funding_status,
            openings: req.body.openings,
            cofounder: req.body.cofounder,
            perks: req.body.perks,
            requirements: req.body.requirements,
            roles: req.body.roles
        };
    var callback = function(err, obj) {
        if(err) {
            console.error(err);
            return res.status(500).send(err);
        }
        res.redirect('/edit-profile');
    };
    services.updateProfile(user, profile, callback);

}
;

exports.shortList = function(req, res, next) {
    var id = req.params.id;
    var user = req.session.user;
    if(!id) {
        return res.status(500).send("Invalid Parameter");
    }
    var callback = function(err, obj) {
        if(err) {
            return res.redirect('/');
        }
        res.send(obj);
    };
    services.shortList(user, id, callback)

};
exports.shortListed = function(req, res, next) {
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).render('shortlisted', {err: err, user: req.session.user});
        }
        return res.status(200).render('shortlisted', {users: obj.starred, user: user});
    };
    services.shortListed(user, callback)

};

exports.removeShortList = function(req, res, next) {
    var id = req.params.id;
    if(!id) {
        return res.status(500).send("Invalid Parameter");
    }
    var user = req.session.user;
    var callback = function(err, obj) {
        if(err) {
            return res.status(500).send(err);
        }
        res.send(obj);
    };
    services.removeShortList(user, id, callback)

};